shelf = 5;
scale = 0.50;

use <shelf.scad>;

scale([scale, scale, scale]) 
translate([0, 0, 26]) {


    rotate([0, 180, 0]) {
        rotate([0,0,90]) {
            translate([-100,0,0]) {
                import("lib/Vise_Base_Jaw_Plate.stl");
            }
        }


        translate([0,0,26 - (shelf / 2)]) {

            shelf(shelf);
        }
    }
}

