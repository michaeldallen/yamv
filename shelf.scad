module shelf(shelf) {
    
    difference() {
        
        // shelf
        color("red") 
            cube([70, shelf, shelf], center = true);

        // center notch
        translate([0, (-1 * (shelf * sqrt(2))) + 1.5, 0])
            rotate([0, 0, 45])
                color("cyan")
                    cube([shelf * 2, shelf * 2, shelf + 1], center=true);

        for(flip = [-1, 1]) {

            // bottom edge chamfer
            translate([flip * (70 / 2) , 0, shelf / 2])
                rotate([0, flip * 45, 0])
                    translate([0, 0, (shelf / 2) - 0.212])
                        color("magenta")
                            cube([shelf,shelf + 1,shelf], center = true);

            // shelf end chamfer
            translate([flip * (70 / 2), 0, 0])
                rotate([0, 0, flip * 45])
                    translate([0,-1 * (shelf - (sqrt(2) * 2) + 2.5455),0])
                        color("blue")
                            cube([shelf * 2, shelf * 2, shelf + 1], center = true);
        }
    }

}

shelf(10);

translate([0,0,10]) shelf(5);
